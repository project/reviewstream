<div class="aggregate">
  <?php echo $ratings_widget; ?>
  <div class="aggregate-text">
    <?php echo $name; ?> is rated <?php echo $ratings_average; ?> out of <?php echo $ratings_max; ?> based on <?php echo $reviews_count; ?> reviews from around the Web.
  </div>
</div>