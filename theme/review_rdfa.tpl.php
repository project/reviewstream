  <div class="review" property="review" typeof="Review">
    <meta property="itemReviewed" content="<?php echo $name; ?>">
    <div class="review-meta">
      <div class="review-date">
        <meta property="datePublished" content="<?php echo $reviewdate; ?>">
        <?php echo $reviewdate; ?>
      </div>
      <div class="review-rating" property="reviewRating" typeof="Rating">
        <meta property="bestRating" content="5">
        <meta property="worstRating" content="1">
        <meta property="ratingValue" content="<?php echo $rating; ?>">
        <?php echo $ratingWidget; ?>
      </div>
    </div>
    <div class="review-text">
      <div class="review-text-inner" property="reviewBody">
      <?php echo $snippet; ?>
      </div>
    </div>
    <div class="review-source">
      <span class="icon-link-<?php echo $category; ?>"></span>
    </div>
    <div class="review-attribution">
      <div class="review-name" property="author">
        <?php echo $attribution; ?>
      </div>
    </div>
    <div class="review-link">
      <a href="<?php echo $url; ?>" target="_blank">View full review here</a>
    </div>
  </div>
