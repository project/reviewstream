<div class="aggregate">
  <?php echo $ratings_widget; ?>
  <div class="aggregate-text">
    <span property="name"><?php echo $name; ?></span> is rated 
    <span property="aggregateRating" typeof="AggregateRating">
      <span property="ratingValue"><?php echo $ratings_average; ?></span> out of 
      <span property="bestRating"><?php echo $ratings_max; ?></span> based on 
      <span property="reviewCount"><?php echo $reviews_count; ?></span> reviews from around the Web.
    </span>
  </div>
</div>