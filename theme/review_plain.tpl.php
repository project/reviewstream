  <div class="review">
    <div class="review-meta">
      <div class="review-date">
        <?php echo $reviewdate; ?>
      </div>
      <div class="review-rating">
        <?php echo $ratingWidget; ?>
      </div>
    </div>
    <div class="review-text">
      <div class="review-text-inner">
      <?php echo $snippet; ?>
      </div>
    </div>
    <div class="review-source">
      <span class="icon-link-<?php echo $category; ?>"></span>
    </div>
    <div class="review-attribution">
      <div class="review-name">
        <?php echo $attribution; ?>
      </div>
    </div>
    <div class="review-link">
      <a href="<?php echo $url; ?>" target="_blank">View full review here</a>
    </div>
  </div>
