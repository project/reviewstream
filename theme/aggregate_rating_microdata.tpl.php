<div class="aggregate">
  <?php echo $ratings_widget; ?>
  <div class="aggregate-text">
    <span itemprop="name"><?php echo $name; ?></span> is rated 
    <span itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
      <span itemprop="ratingValue"><?php echo $ratings_average; ?></span> out of 
      <span itemprop="bestRating"><?php echo $ratings_max; ?></span> based on 
      <span itemprop="reviewCount"><?php echo $reviews_count; ?></span> reviews from around the Web.
    </span>
  </div>
</div>