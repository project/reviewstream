  <div class="review" itemprop="review" itemscope itemtype="http://schema.org/Review">
    <meta itemprop="itemReviewed" content="<?php echo $name; ?>">
    <div class="review-meta">
      <div class="review-date">
        <meta itemprop="datePublished" content="<?php echo $reviewdate; ?>">
        <?php echo $reviewdate; ?>
      </div>
      <div class="review-rating" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
        <meta itemprop="bestRating" content="5">
        <meta itemprop="worstRating" content="1">
        <meta itemprop="ratingValue" content="<?php echo $rating; ?>">
        <?php echo $ratingWidget; ?>
      </div>
    </div>
    <div class="review-text">
      <div class="review-text-inner" itemprop="reviewBody">
      <?php echo $snippet; ?>
      </div>
    </div>
    <div class="review-source">
      <span class="icon-link-<?php echo $category; ?>"></span>
    </div>
    <div class="review-attribution">
      <div class="review-name" itemprop="author">
        <?php echo $attribution; ?>
      </div>
    </div>
    <div class="review-link">
      <a href="<?php echo $url; ?>" target="_blank">View full review here</a>
    </div>
  </div>
