<div itemscope itemtype="http://schema.org/<?php echo $type; ?>" id="reviewstream" class="<?php echo $streamclass; ?>">
<meta itemprop="name" content="<?php echo $name; ?>">
<meta itemprop="image" content="<?php echo $image_url; ?>">
<?php echo $content; ?>
<div id="reviewstream-pager"><?php echo $pager; ?></div>
<div id="reviewstream-footer"><?php echo $powered_by; ?></div>
</div>