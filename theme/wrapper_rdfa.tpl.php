<div vocab="http://schema.org" typeof="<?php echo $type; ?>" id="reviewstream" class="<?php echo $streamclass; ?>">
<meta property="name" content="<?php echo $name; ?>">
<meta property="image" content="<?php echo $image_url; ?>">
<?php echo $content; ?>
<div id="reviewstream-pager"><?php echo $pager; ?></div>
<div id="reviewstream-footer"><?php echo $powered_by; ?></div>
</div>