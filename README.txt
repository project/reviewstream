Review Stream
==========

Stream your latest and greatest reviews from around the Web to your Drupal 
site and display them with SEO-friendly rich-snippet markup.


Dependencies and incompatibilities
============

* None at present


Installation and Usage
======================

To use this plugin, you must:

* have a Grade.us account with review monitoring enabled
* be approved for API access and know your API token

1) Copy the reviewstream folder to the modules folder in your installation.

2) Enable the module (/admin/modules).

3) Grant the 'administer review stream' permission to selected roles and visit
   the configuration page at admin > config > content > reviewstream
   to choose enter your API credentials and set your review display options.

4) Add the Review Stream reviews block, or use the [node:reviewstream] token if
   your site is configured to use tokens.
   
